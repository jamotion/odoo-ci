#!/bin/bash
sudo /etc/init.d/postgresql start
while true
do 
  psql -U odoo -t -c 'SELECT pg_postmaster_start_time()' 2> /dev/null
  if [[ $? == 0 ]]
  then
    break
  fi
  sleep 1
done
