#!/bin/bash
sudo cp -r /opt/atlassian/bitbucketci/agent/build /opt/odoo/custom_addons/$BITBUCKET_REPO_SLUG
sudo chown -R odoo.odoo /opt/odoo
cd ~
mkdir ~/.ssh
echo $JAMO_SSH_PKEY | base64 --decode > ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa
chmod 700 ~/.ssh
echo $BITBUCKET_KNOWN_HOST > ~/.ssh/known_hosts
echo $GITHUB_KNOWN_HOST >> ~/.ssh/known_hosts
